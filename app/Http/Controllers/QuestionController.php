<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Question;
class QuestionController extends Controller


{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('questions')->get();

        return view('questions.index', [
            'datas' => $datas
        ])->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required|max:255',
        ]);

        DB::table('questions')->insert([
            'judul' => $request->judul,
            'isi' => $request->isi
        ]);


        return redirect()->route('questions.index')->with('success', 'Questions berhasil dibuat!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = DB::table('questions')->where('id', $id)->first();

        return view('questions.show', ['questions' => $questions]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = DB::table('questions')->where('id', $id)->first();

        return view('questions.edit', ['question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required|max:255',
        ]);

        DB::table('questions')->where('id', $id)->update([
            'judul' => $request->judul,
            'isi' => $request->isi
        ]);


        return redirect()->route('questions.index')->with('success', 'Questions berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('questions')->where('id', $id)->delete();

        toastr()->success('Success!', 'Questions deleted!');
        return redirect()->route('questions.index');
    }
}
