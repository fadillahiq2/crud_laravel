@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-md-11">



        <!-- About Me Box -->
        <div class="card card-primary mt-5 ml-5">
          <div class="card-header">
            <h3 class="card-title pt-2">Show</h3>
            <div class="d-flex justify-content-end">
                <a class="btn btn-primary" href="{{ route('questions.index') }}"> Back</a>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <strong><i class="fas fa-pencil-alt mr-1"></i> Judul</strong>

            <p class="text-muted">
              <span class="tag tag-danger">{{ $questions->judul }}</span>
            </p>

            <hr>

            <strong><i class="far fa-file-alt mr-1"></i> Isi</strong>

            <p class="text-muted">{{ $questions->isi }}</p>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
</div>
@endsection
