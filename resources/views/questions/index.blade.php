@extends('layouts.master')

@section('content')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>List Questions</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('questions.index') }}">Home</a></li>
            <li class="breadcrumb-item active">List Questions</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><a class="btn btn-primary" href="{{ route('questions.create') }}">New Questions</a></h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th width="280px">Action</th>
              </tr>
              </thead>
              <tbody>
            @forelse ($datas as $key => $data )
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $data->judul }}</td>
                <td>{{ $data->isi }}</td>
                <td>
                    <form action="{{ route('questions.destroy',$data->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('questions.show',$data->id) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('questions.edit',$data->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
              </tr>
            @empty
            <tr>
                <td colspan="4" class="text-center"> Data belum ada </td>
            </tr>
            @endforelse
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
</div>
@endsection
